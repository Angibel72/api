//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html')); //dirname dice que lo tome de donde estoy parado.
});

app.get("/clientes/:idcliente", function(req, res) {
  res.send('Aqui tiene el cliente número :' + req.params.idcliente);
});

app.post("/", function(req, res) {
  res.send('Hemos recibido su petición cambiada');
});

app.put("/", function(req, res) {
  res.send('Hemos recibido su petición de actualización');
});

app.delete("/", function(req, res) {
  res.send('Hemos recibido su petición de borrado');
});
